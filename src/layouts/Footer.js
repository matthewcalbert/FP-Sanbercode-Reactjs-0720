import React, { useState } from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const MyFooter = props =>{
  return (
    <Footer style={{ textAlign: 'center' }}>Final Project Sanbercode ReactJS @{new Date().getFullYear()} By Matthew Christopher Albert</Footer>
  )
}

export default MyFooter;