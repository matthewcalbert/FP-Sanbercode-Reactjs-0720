import React, { useState } from 'react';
import { Layout, Button, Breadcrumb } from 'antd';
import MyHeader from './Header';
import MyFooter from './Footer';
import Routes from '../Routes';
import Sidebar from './Sidebar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

const { Content } = Layout;

function FaWrapper(props){
    return <span role="img" className="anticon" style={{marginRight:10}}>{props.children}</span>
}

const MyLayout = () => {
    const [collapsed, setCollapsed] = useState(false);

    const padLeft = ()=>{
        if (collapsed) return "site-layout collapsed";
        return "site-layout";
    } 

    const padLeftToggler = ()=>{
        if (collapsed) return "toggler-icon collapsed";
        return "toggler-icon";
    } 
    
    const toggleSidebar = ()=>{
        setCollapsed(!collapsed);
    }

    const getTogglerIcon = ()=>{ 
        if(collapsed) return (<FaWrapper><FontAwesomeIcon icon={faBars} size="2x"/></FaWrapper>); else return (<FaWrapper><FontAwesomeIcon icon={faTimes} size="2x"/></FaWrapper>); 
    }
    
    return (
        <Layout>
            <MyHeader>
                <span className={padLeftToggler()} onClick={toggleSidebar}>{getTogglerIcon()}</span>
            </MyHeader>
            <Sidebar collapsed={collapsed}></Sidebar>
            <Content className={padLeft()} style={{ marginTop: 64 }}>
                <div className="content">
                    <Routes></Routes>
                </div>
                <MyFooter></MyFooter>
            </Content>
        </Layout>
    );
};

export default MyLayout;