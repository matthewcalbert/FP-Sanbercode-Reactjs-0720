import React, { useState, useContext, useEffect } from 'react';
import {Link, Redirect, useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import { Layout, Menu, Button } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGamepad, faFilm } from '@fortawesome/free-solid-svg-icons';
import {
    UnorderedListOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    HomeOutlined,
  } from '@ant-design/icons';
import { AuthContext } from '../AuthContext';

const { SubMenu } = Menu;

function FaWrapper(props){
    return <span role="img" className="anticon" style={{marginRight:10}}>{props.children}</span>
}

const Sidebar = props => {
    const history = useHistory();
    let location = useLocation();
    const [currentLogin, setLogin] = useContext(AuthContext);
    const [currentMenu, setMenu] = useState("");
    const collapsed = props.collapsed;

    const matchCat = useRouteMatch('/:cat/:cat2');


    useEffect(()=>{
        if (location.pathname === "/"){
            setMenu("home");
        }else{
            switch(location.pathname){
                case "/movies": setMenu("mvlist"); break;
                case "/movies/table": setMenu("mvtable"); break;
                case "/movies/edit": setMenu("mvtable"); break;
                case "/movies/create": setMenu("mvtable"); break;
                case "/games": setMenu("gmlist"); break;
                case "/games/table": setMenu("gmtable"); break;
                case "/games/edit": setMenu("mvtable"); break;
                case "/games/create": setMenu("gmtable"); break;
                default: break;
            }
            if( matchCat ){
              if( !isNaN(parseInt(matchCat.params.cat2)) ){
                if( matchCat.params.cat === "movies" ){
                  setMenu('mvlist');
                }if( matchCat.params.cat === "games" ){
                  setMenu('gmlist');
                }
              }
            }
        }
    },[location])

    return (
        <>
        <div className="sidebar">
          <Menu
            selectedKeys={currentMenu}
            defaultOpenKeys={['sub1']}
            mode="inline"
            theme="dark"
            inlineCollapsed={collapsed}
          >
            <Menu.Item key="home" icon={<HomeOutlined />}>
                <Link to="/">Home</Link>
            </Menu.Item>
            <SubMenu key="movies" icon={<FaWrapper><FontAwesomeIcon icon={faFilm} /></FaWrapper>} title="Movies">
                <Menu.Item key="mvlist"><Link to="/movies">List</Link></Menu.Item>
                <Menu.Item key="mvtable"><Link to="/movies/table">Table</Link></Menu.Item>
            </SubMenu>
            <SubMenu key="games" icon={<FaWrapper><FontAwesomeIcon icon={faGamepad} /></FaWrapper>} title="Games">
                <Menu.Item key="gmlist"><Link to="/games">List</Link></Menu.Item>
                <Menu.Item key="gmtable"><Link to="/games/table">Table</Link></Menu.Item>
            </SubMenu>
          </Menu>
        </div>
        </>
    )
}

export default Sidebar;