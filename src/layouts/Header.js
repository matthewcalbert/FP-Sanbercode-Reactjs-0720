import React, { useState, useContext, useEffect } from 'react';
import {Link, Redirect, useHistory, useLocation, useRouteMatch} from 'react-router-dom';
import { Layout, Menu, Button } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKey, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import {
    UserOutlined,
    LoginOutlined
} from '@ant-design/icons';
import { AuthContext } from '../AuthContext';

const { Header } = Layout;
const { SubMenu } = Menu;

function FaWrapper(props){
    return <span role="img" className="anticon" style={{marginRight:10}}>{props.children}</span>
}

const MyHeader = props => {
    const history = useHistory();
    let location = useLocation();
    const [currentLogin, setLogin] = useContext(AuthContext);
    const [currentMenu, setMenu] = useState();

    const matchCat = useRouteMatch('/:cat/:cat2');

    const handleLogout = ()=>{
        localStorage.removeItem("session")
        setLogin(null);
        history.push("/");
    }

    let authLink = ""
    if( currentLogin === null ){
        authLink = [
            (<Menu.Item key="/register"><Link to="/register">Register</Link></Menu.Item>)
            ,
            (<Menu.Item key="/login" icon={<LoginOutlined />}><Link to="/login">Login</Link></Menu.Item>)
        ]
    }else{
        authLink = (
            <SubMenu key="auth" icon={<UserOutlined />} title="Account">
                <Menu.Item key="chp" icon={<FaWrapper><FontAwesomeIcon icon={faKey} /></FaWrapper>}><Link to="/change-password">Change Password</Link></Menu.Item>
                <Menu.Item key="5" onClick={handleLogout} icon={<FaWrapper><FontAwesomeIcon icon={faSignOutAlt} /></FaWrapper>}>Logout</Menu.Item>
            </SubMenu>
        )
    }


    useEffect(()=>{
        if (location.pathname === "/"){
            setMenu("home");
        }else{
            switch(location.pathname){
                case "/register": setMenu("/register"); break;
                case "/login": setMenu("/login"); break;
                case "/change-password": setMenu("chp"); break;
                default: setMenu(""); break;
            }
        }
    },[location])

    return (
        <>
        <Header className="header" theme="light">
            <div style={{"color":"white"}}>
                {props.children}
                <div className="logo">
                    <Link to="/" style={{zIndex:-1, userSelect:"none"}}><img draggable="false" src="/rxsnbr.png" width="200px"></img></Link>
                </div>
            </div>
            <Menu theme="light" mode="horizontal" selectedKeys={currentMenu}>
                {authLink}
            </Menu>
        </Header>
        </>
    )
}

export default MyHeader;