import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router";
import { Layout, Menu, Breadcrumb } from 'antd';
import { AuthContext } from './AuthContext';
import Login from "./auth/Login";
import ChangePassword from "./auth/ChangePassword";
import Register from "./auth/Register";
import GamesDataEditor from "./games/Table";
import CreateGame from "./games/Create";
import MoviesDataEditor from "./movies/Table";
import EditGame from "./games/Edit";
import GamesList from "./games/List";
import MovieList from "./movies/List";
import EditMovie from "./movies/Edit";
import CreateMovie from "./movies/Create";
import { Home } from "./Home";
import GameDetail from "./games/Detail";
import MovieDetail from "./movies/Detail";

const Routes = () => {
    const [currentLogin,setLogin] = useContext(AuthContext);

    const PrivateRoute = ({user, ...props }) => {
        if (user) {
        return <Route {...props} />;
        } else {
        return <Redirect to="/login" />;
        }
    };

    const LoginRoute = ({user, ...props }) =>
    user ? <Redirect to="/" /> : <Route {...props} />;


    return (
        <Switch>
            <Route exact path="/">
                <Home user={currentLogin}></Home>
            </Route>

            <Route exact path="/games" component={GamesList}/>
            <Route exact path="/movies" component={MovieList}/>

            {/* Auth Routes */}
            <PrivateRoute user={currentLogin} exact path="/games/table" component={GamesDataEditor}/>
            <PrivateRoute user={currentLogin} exact path="/games/create" component={CreateGame}/>
            <PrivateRoute user={currentLogin} path="/games/edit/:id" component={EditGame}/>

            <PrivateRoute user={currentLogin} exact path="/movies/table" component={MoviesDataEditor}/>
            <PrivateRoute user={currentLogin} exact path="/movies/create" component={CreateMovie}/>
            <PrivateRoute user={currentLogin} path="/movies/edit/:id" component={EditMovie}/>

            <LoginRoute user={currentLogin} exact path="/login" component={Login}/>
            <PrivateRoute user={currentLogin} exact path="/change-password" component={ChangePassword}/>
            <LoginRoute user={currentLogin} exact path="/register" component={Register}/>

            {/* Detail Route */}
            <Route path="/games/:id" component={GameDetail}/>
            <Route path="/movies/:id" component={MovieDetail}/>
        </Switch>
    );

};

export default Routes;