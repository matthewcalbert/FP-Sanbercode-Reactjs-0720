import React from 'react';
import { Card, Row, Col } from 'antd';
import { Link } from 'react-router-dom';

export function Home(props){
    document.title = "Final Project Sanbercode ReactJS";

    const welcome = ()=>{
        if( props.user !== null ){
            return (<p>It appear that you're logged in using <b>{props.user.username}</b>.</p>)
        }
        return (<p>It appear that you haven't login. <Link to="/login">Click here to login</Link></p>)
    }

    return(
        <>
        <div className="site-layout-background" style={{ padding: 24, minHeight: '600px' }}>
            <Row align="center" justify="center">
                <Col md={18} sm={24} lg={12}>
                    <Card title="Hello, Welcome to My Final Project Sanbercode ReactJS" bordered={false}>
                        {welcome()}
                    </Card>
                </Col>
            </Row>
        </div>
        </>
    )
}