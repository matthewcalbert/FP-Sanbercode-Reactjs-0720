import React, { useEffect, useContext, useState } from 'react';
import axios from 'axios';
import { PageHeader, Form, Input, Button, Alert, Checkbox, Skeleton, message, Rate  } from 'antd';
import { Row, Col } from 'antd';
import { useRouteMatch } from 'react-router-dom';

const { TextArea } = Input;

const EditMovie = ()=>{
    
    const matchId = useRouteMatch('/movies/edit/:id').params.id;
    document.title = "Edit Movie #"+matchId;
    let previousData = null;

    const [dataLoaded, setDataLoaded] = useState(false);

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    useEffect( ()=>{
        if( previousData === null ) fetchData();
        forceUpdate({});
    },[] )

    const fetchData = (callback = ()=>{})=>{
        axios.get(`https://backendexample.sanbersy.com/api/movies/${matchId}`)
        .then(res => {
            previousData = res.data;
            form.setFieldsValue({
                title: previousData.title, genre: previousData.genre, year: parseInt(previousData.year), rating: parseInt(previousData.rating)/2, description: previousData.description, duration: previousData.duration, review: previousData.review, image_url: previousData.image_url
            })
            setDataLoaded(true);
            callback(res.data);
        });
    }

    const handleSubmit = (e)=>{
        let params = {...e}
        axios.put(`https://backendexample.sanbersy.com/api/movies/${matchId}`,{
            title: params.title, genre: params.genre, year: parseInt(params.year), rating: parseInt(params.rating)*2, description: params.description, review: params.review, duration: parseInt(params.duration), image_url: params.image_url
        })
        .then(res => {
            message.success("Movie succesfully edited")
            console.log(res.data);
        });
    }

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    };

    return (
        <>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title={"Edit Movie #"+matchId}
          >
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
                <Form {...layout} form={form} name="create_movie" onFinish={handleSubmit} initialValues={{ remember: true }}>
                <Form.Item
                    name={`title`}
                    label={`Title`}
                    rules={[{ required: true, message: 'Please input your new movie title!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`genre`}
                    label={`Genre`}
                    rules={[{ required: true, message: 'Please input your new movie genre!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`year`}
                    label={`Year`}
                    rules={[{ required: true, message: 'Please input your new movie release year!' }]}
                >
                    <Input type="number" min="0" placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`duration`}
                    label={`Duration`}
                    rules={[{ required: true, message: 'Please input your new movie duration!' }]}
                >
                    <Input type="number" min="0" placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`description`}
                    label={`Description`}
                >
                    <TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder="" />
                </Form.Item>
                <Form.Item name="rating" label="Rating"
                    rules={[{ required: true, message: 'Please input your new movie rating!' }]}
                >
                    <Rate allowHalf ></Rate>
                </Form.Item>
                <Form.Item
                    name={`review`}
                    label={`Review`}
                >
                    <TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`image_url`}
                    label={`Image URL`}
                    rules={[
                        {
                            type: "url",
                            message: "This field must be a valid url."
                        }
                    ]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item {...tailLayout} shouldUpdate={true}>
                {() => (
                    <Button
                    type="primary"
                    htmlType="submit"
                    disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                    }
                    >
                    Update
                    </Button>
                )}
                </Form.Item>
                </Form>
            </Col>
          </Row>
        </div>
        </>
    );
}

export default EditMovie;