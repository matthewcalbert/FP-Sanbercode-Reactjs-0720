import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import { Tag, Rate, Card, Row, Col, PageHeader, Skeleton } from 'antd';

class MovieDetail extends React.Component{
    constructor(props){
        super(props);
        this.movieId = this.props.match.params.id;
        this.state = {data: null, dataLoaded: false};
        if( this.state.data === null ){
            this.fetchData();
        }
        document.title =  "Movie #"+this.movieId;
    }

    fetchData = ()=>{
        axios.get(`https://backendexample.sanbersy.com/api/movies/${this.movieId}`)
        .then(res => {
            let rdata = res.data;
            this.setState({
                data: rdata,
                dataLoaded: true
            });
        });
    }

    renderPost(){
        if( !this.state.dataLoaded ){
            return (
                <>
                <Card bordered={false}className="post" style={{minWidth:300}}>
                <Skeleton avatar paragraph={{ rows: 4 }} />
                </Card>
                </>
                )
        }
        if( this.state.data !== null ){
            let el = this.state.data;
            document.title = el.title+" | Movie";
            let durho = Math.floor(el.duration/60);
            let durmin = el.duration%60;
            durho = durho === 0 ? "" : durho+= " jam";
            durmin = durmin === 0 ? "" : durmin+= " menit";
            return (
                <Card key={el.id} title={el.title} bordered={false}className="post">
                    <img src={el.image_url} className="post-image"></img>
                    <p><b>Genre: </b>{el.genre}</p>
                    <p><b>Tahun Rilis: </b>{el.year}</p>
                    <p><b>Durasi: </b>{durho} {durmin}</p>
                    <p><b>Deskripsi: </b> {el.description}</p>
                    <p><b>Rating: </b><Rate disabled allowHalf defaultValue={el.rating/2} /></p>
                    <p><b>Review: </b> {el.review}</p>
                </Card>
            )
        }
    }

    render(){
        return (
            <>
            <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title={this.state.data !== null && this.state.data.title+" | Movie"}
            >
            </PageHeader>
            <Row align="center" justify="center" style={{marginTop:30}}>
              <Col>
              {this.renderPost()}
              </Col>
            </Row>
            </>
        )
    }
}

export default MovieDetail;