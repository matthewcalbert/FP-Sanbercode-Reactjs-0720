import React, { useEffect, useContext, useState } from 'react';
import axios from 'axios';
import { PageHeader, Form, Input, Button, Alert, Checkbox, InputNumber, message  } from 'antd';
import { CheckCircleOutlined, CloseCircleOutlined, EditOutlined, DeleteOutlined, DownOutlined, UpOutlined, PlusOutlined } from '@ant-design/icons';
import { Row, Col } from 'antd';
import { Table, Tag, Space } from 'antd';
import { Link } from 'react-router-dom';
import * as moment from 'moment'

function compareByAlph (a, b) { if (a < b) { return -1; } if (a > b) { return 1; } return 0; }

const MoviesDataEditor = ()=>{
    document.title = "Movies Table";

    const [data,setData] = useState(null);

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    const fetchData = (callback = ()=>{})=>{
        axios.get(`https://backendexample.sanbersy.com/api/movies`)
        .then(res => {
            setData(res.data)
            console.log(res.data)
            callback(res.data);
        });
    }

    useEffect( ()=>{
        forceUpdate({});
        if( data === null ){
            fetchData();
        }
    },[] )

    const DeleteButton = ({itemId})=>{
      const handleDelete = ()=>{
          let id = itemId;
          axios.delete(`https://backendexample.sanbersy.com/api/movies/${id}`)
          .then(res => {
              if (res.data === "success"){
                  let newArr = data;
                  newArr = newArr.filter(x => x.id !== parseInt(id));
                  setData([...newArr]);
                  message.success('Movie deleted');
                  //refetch method
                  // fetchData();
              }else{
                  message.warning('Failed to delete movie');
              }
          });
      }
      return (<a onClick={handleDelete}><DeleteOutlined /></a>)
    }

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            sorter: {
              compare: (a, b) => a.id - b.id,
              multiple: 1,
            },
          },
        {
          title: 'Title',
          dataIndex: 'title',
          key: 'title',
          sorter: (a, b) => compareByAlph(a.title, b.title),
        },
        {
          title: 'Genre',
          dataIndex: 'genre',
          key: 'genre',
        },
        {
          title: 'Year',
          key: 'year',
          dataIndex: 'year',
          sorter: {
            compare: (a, b) => a.year - b.year,
            multiple: 1,
          },
        },
        {
          title: 'Duration',
          key: 'duration',
          dataIndex: 'duration',
          sorter: {
            compare: (a, b) => a.duration - b.duration,
            multiple: 1,
          },
        },
        {
          title: 'Rating',
          key: 'rating',
          dataIndex: 'rating',
          sorter: {
            compare: (a, b) => a.rating - b.rating,
            multiple: 1,
          },
        },
        {
          title: 'Created At',
          key: 'created_at',
          dataIndex: 'created_at',
          sorter: (a, b) => { return moment(a.created_at).unix() - moment(b.created_at).unix()}
        },
        {
          title: 'Updated At',
          key: 'updated_at',
          dataIndex: 'updated_at',
          sorter: (a, b) => { return moment(a.updated_at).unix() - moment(b.updated_at).unix()}
        },
        {
          title: 'Action',
          key: 'action',
          fixed: 'right',
          render: (text, record) => (
            // {record.name}
            <Space size="middle">
              <Link to={'/movies/edit/'+record.id}><EditOutlined /></Link>
              <DeleteButton itemId={record.id}></DeleteButton>
            </Space>
          ),
        },
      ];
    
      const handleSearch = values => {
        let param = {...values};

        const filterate = (x)=>{
            if( param.title !== undefined ){
                if( x.title.toLowerCase().search(param.title.toLowerCase()) === -1 )return;
            }

            if( param.year_start !== undefined && !isNaN(parseFloat(param.year_start)) ){
                if( parseInt(x.year) < parseInt(param.year_start) )return;
            }
            if( param.year_end !== undefined && !isNaN(parseFloat(param.year_end)) ){
                if( parseInt(x.year) > parseInt(param.year_end)  )return;
            }
            
            if( param.rating_start !== undefined && !isNaN(parseFloat(param.rating_start)) ){
                if( parseInt(x.rating) < parseInt(param.rating_start) )return;
            }
            if( param.rating_end !== undefined && !isNaN(parseFloat(param.rating_end)) ){
                if( parseInt(x.rating) > parseInt(param.rating_end)  )return;
            }

            if( param.genre !== undefined ){
                if( x.genre.toLowerCase().search(param.genre.toLowerCase()) === -1 )return;
            }
            if( param.platform !== undefined ){
                if( x.platform.toLowerCase().search(param.platform.toLowerCase()) === -1 )return;
            }
            return x;
        }

        fetchData(function(d){
            let newArr = d;
            newArr = newArr.filter(x => filterate(x));
            setData([...newArr]);
        });
        //refetch method
        // fetchData(); 
      };

    return (
        <>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Movies Table"
          >
          </PageHeader>
            <Form
            form={form}
            name="advanced_search"
            className="ant-advanced-search-form"
            onFinish={handleSearch}
            >
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item
                            name={`title`}
                            label={`Title`}
                        >
                            <Input placeholder="Search movie title..." />
                        </Form.Item>
                    </Col>
                    <Col>
                      <Form.Item
                          label={`Year`}
                      >
                        <Input.Group compact>
                          <Form.Item
                              name={`year_start`}
                              style={{margin:0}}
                              rules={[({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if ( (value && getFieldValue('year_end') ) && value > getFieldValue('year_end') ) {
                                    return Promise.reject('Min cannot exceed max');
                                  }
                                  return Promise.resolve();
                                },
                              }),]}
                          >
                            <Input style={{ width: 70, textAlign: 'center', borderRight:0 }} placeholder="Min" type="number" min="0" />
                                
                          </Form.Item>
                          <Input
                            className="site-input-split"
                            style={{
                              width: 30,
                              borderLeft: 0,
                              borderRight: 0,
                              pointerEvents: 'none',
                              backgroundColor: '#fff'
                            }}
                            placeholder="~"
                            disabled
                          />
                          <Form.Item
                              name={`year_end`}
                              style={{margin:0}}
                              rules={[({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if ( (value && getFieldValue('year_start') ) && value < getFieldValue('year_start') ) {
                                    return Promise.reject('Max cannot be less than min');
                                  }
                                  return Promise.resolve();
                                },
                              }),]}
                          >
                            <Input
                              className="site-input-right"
                              style={{
                                width: 70,
                                borderLeft: 0,
                                textAlign: 'center',
                              }}
                              placeholder="Max" type="number" min="0"
                            />
                          </Form.Item>
                        </Input.Group>
                      </Form.Item>
                    </Col>
                    <Col>
                      <Form.Item
                          label={`Rating`}
                      >
                        <Input.Group compact>
                          <Form.Item
                              name={`rating_start`}
                              style={{margin:0}}
                              rules={[({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if ( (value && getFieldValue('rating_end') ) && value > getFieldValue('rating_end') ) {
                                    return Promise.reject('Min cannot exceed max');
                                  }
                                  return Promise.resolve();
                                },
                              }),]}
                          >
                            <Input style={{ width: 70, textAlign: 'center', borderRight:0 }} placeholder="Min" type="number" min="0" max="10" />
                                
                          </Form.Item>
                          <Input
                            className="site-input-split"
                            style={{
                              width: 30,
                              borderLeft: 0,
                              borderRight: 0,
                              pointerEvents: 'none',
                              backgroundColor: '#fff'
                            }}
                            placeholder="~"
                            disabled
                          />
                          <Form.Item
                              name={`rating_end`}
                              style={{margin:0}}
                              rules={[({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if ( (value && getFieldValue('rating_start') ) && value < getFieldValue('rating_start') ) {
                                    return Promise.reject('Max cannot be less than min');
                                  }
                                  return Promise.resolve();
                                },
                              }),]}
                          >
                            <Input
                              className="site-input-right"
                              style={{
                                width: 70,
                                borderLeft: 0,
                                textAlign: 'center',
                              }}
                              placeholder="Max" type="number" min="0" max="10"
                            />
                          </Form.Item>
                        </Input.Group>
                      </Form.Item>
                    </Col>
                    <Col>
                        <Form.Item
                            name={`genre`}
                            label={`Genre`}
                        >
                            <Input placeholder="All" />
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                    <Button
                        style={{ margin: '0 8px' }}
                        onClick={() => {
                        form.resetFields();
                        }}
                    >
                        Clear
                    </Button>
                    <Button type="primary" htmlType="button">
                        <Link to="/movies/create"><PlusOutlined /> Add Movies</Link>
                    </Button>
                    </Col>
                </Row>
            </Form>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col span="12">
            </Col>
            <Col style={{"minWidth":"400px"}}>
                <Table dataSource={data} columns={columns}  scroll={{x:1000, y: 400 }}  />
            </Col>
          </Row>
        </div>
        </>
    )
}

export default MoviesDataEditor;