import React, { useEffect, useContext, useState } from 'react';
import axios from 'axios';
import { PageHeader, Form, Input, Button, Alert, Checkbox, Rate, message  } from 'antd';
import { Row, Col } from 'antd';

const { TextArea } = Input;

const CreateMovie = ()=>{
    document.title = "Add a Movie";

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    useEffect( ()=>{
        forceUpdate({});
    },[] )

    const handleSubmit = (e)=>{
        let params = {...e}
        axios.post(`https://backendexample.sanbersy.com/api/movies`,{
            title: params.title, genre: params.genre, year: parseInt(params.year), rating: parseInt(params.rating)*2, description: params.description, review: params.review, duration: parseInt(params.duration), image_url: params.image_url
        })
        .then(res => {
            form.resetFields();
            message.success("Movie succesfully added")
            console.log(res.data);
        });
    }

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    };

    return (
        <>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Add a Movie"
          >
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
              <Form {...layout} form={form} name="create_movie" onFinish={handleSubmit} initialValues={{ remember: true }}>
                <Form.Item
                    name={`title`}
                    label={`Title`}
                    rules={[{ required: true, message: 'Please input your new movie title!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`genre`}
                    label={`Genre`}
                    rules={[{ required: true, message: 'Please input your new movie genre!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`year`}
                    label={`Year`}
                    rules={[{ required: true, message: 'Please input your new movie release year!' }]}
                >
                    <Input type="number" min="0" placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`duration`}
                    label={`Duration`}
                    rules={[{ required: true, message: 'Please input your new movie duration!' }]}
                >
                    <Input type="number" min="0" placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`description`}
                    label={`Description`}
                >
                    <TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder="" />
                </Form.Item>
                <Form.Item name="rating" label="Rating"
                    rules={[{ required: true, message: 'Please input your new movie rating!' }]}
                >
                    <Rate allowHalf ></Rate>
                </Form.Item>
                <Form.Item
                    name={`review`}
                    label={`Review`}
                >
                    <TextArea autosize={{ minRows: 2, maxRows: 6 }} placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`image_url`}
                    label={`Image URL`}
                    rules={[
                        {
                            type: "url",
                            message: "This field must be a valid url."
                        }
                    ]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item {...tailLayout} shouldUpdate={true}>
                  {() => (
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                      }
                    >
                      Submit
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>
        </>
    )
}

export default CreateMovie;