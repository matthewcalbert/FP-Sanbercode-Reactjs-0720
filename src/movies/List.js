import React, { useState } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import { Tag, Divider, Card, Row, Col, PageHeader, Skeleton, Rate, Button } from 'antd';


const ToggleableDescription = (props)=>{
    const [collapsed, setCollapsed] = useState(true);

    const toggleCollapse = ()=>{
        setCollapsed(!collapsed);
    }

    const renderDecision = ()=>{
        if( collapsed ){
            if( props.content !== undefined ){
                return (
                <>{props.content && props.content.substr(0,250)}
                {props.content && props.content.length > 250 ? (<>.. <Link onClick={toggleCollapse}>Show more</Link></>) : "" }
                </>
                )
            }
        }else{
            return (
            <>{props.content}
            <Link onClick={toggleCollapse} style={{marginLeft:5}}>Show less</Link>
            </>
            )
        }
    }

    return (<>
        {renderDecision()}
    </>)
}

class MovieList extends React.Component{
    constructor(props){
        super(props);
        this.state = {data: null, dataLoaded: false};
        if( this.state.data === null ){
            this.fetchData();
        }
        document.title = "List Movies";
    }

    fetchData = ()=>{
        axios.get(`https://backendexample.sanbersy.com/api/movies`)
        .then(res => {
            console.log(res.data)
            let rdata = res.data;
            this.setState({
                data: rdata,
                dataLoaded: true
            });
        });
    }

    renderPost(){
        if( !this.state.dataLoaded ){
            return (
                <>
                <Card bordered={false}className="post" style={{minWidth:300}}>
                    <Skeleton avatar paragraph={{ rows: 4 }} active/>
                </Card>
                <Card bordered={false}className="post" style={{minWidth:300}}>
                    <Skeleton avatar paragraph={{ rows: 4 }} active/>
                </Card>
                </>
                )
        }
        return <>{
            this.state.data !== null && this.state.data.map((el)=>{
                let durho = Math.floor(el.duration/60);
                let durmin = el.duration%60;
                durho = durho === 0 ? "" : durho+= " jam";
                durmin = durmin === 0 ? "" : durmin+= " menit";
                return (
                    <Card key={el.id} title={el.title} bordered={false}className="post">
                        <img src={el.image_url} className="post-image"></img>
                        <p><b>Genre: </b>{el.genre}</p>
                        <p><b>Tahun Rilis: </b>{el.year}</p>
                        <p><b>Durasi: </b>{durho} {durmin}</p>
                        <p><b>Deskripsi: </b><ToggleableDescription content={el.description} /> </p>
                        <p><b>Rating: </b><Rate disabled allowHalf defaultValue={el.rating/2} /></p>
                        <p><b>Review: </b> {el.review && el.review.substr(0,100)} {el.review && el.review.length > 100 ? "...":""}</p>
                        <Row justify="end">
                            <Button type="primary"><Link to={"/movies/"+el.id}>Detail Review</Link></Button>
                        </Row>
                    </Card>
                )
            })
        }</>
    }

    render(){
        return (
            <>
            <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="List Movies"
            >
            </PageHeader>
            <Row align="center" justify="center" style={{marginTop:30}}>
              <Col>
              {this.renderPost()}
              </Col>
            </Row>
            </>
        )
    }
}

export default MovieList;