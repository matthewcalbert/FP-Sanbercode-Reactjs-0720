import React, { useState, createContext } from 'react';

export const AuthContext = createContext();

export const AuthProvider = props =>{
    const c = ()=>{
        try{
            let x = JSON.parse(localStorage.getItem("session"));
            return x;
        }
        catch(e){
            return null;
        }
    }
    const [currentLogin,setLogin] = useState(c());

    return (
        <AuthContext.Provider value={[currentLogin,setLogin]}>
            {props.children}
        </AuthContext.Provider>
    )
}