import React, { useEffect, useContext, useState } from 'react';
import axios from 'axios';
import { PageHeader, Form, Input, Button, Alert, Checkbox, InputNumber, message  } from 'antd';
import { Row, Col } from 'antd';

const CreateGame = ()=>{
    document.title = "Add a Game";

    const [formCheckbox, setFormCheckbox] = useState({mp:false, sp:false});

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    useEffect( ()=>{
        forceUpdate({});
    },[] )

    const handleSubmit = (e)=>{
        let params = {...e, ...formCheckbox}
        axios.post(`https://backendexample.sanbersy.com/api/games`,{
            name: params.name, genre: params.genre, release: parseInt(params.release), platform: params.platform, singlePlayer: params.sp, multiplayer: params.mp, image_url: params.image_url
        })
        .then(res => {
            form.resetFields();
            message.success("Game succesfully added")
            console.log(res.data);
        });
    }

    const handleMp = e =>{
        setFormCheckbox({...formCheckbox, mp: e.target.checked})
    }
    const handleSp = e =>{
        setFormCheckbox({...formCheckbox, sp: e.target.checked})
    }

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    };

    return (
        <>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Add a Game"
          >
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
              <Form {...layout} form={form} name="create_games" onFinish={handleSubmit} initialValues={{ remember: true }}>
                <Form.Item
                    name={`name`}
                    label={`Name`}
                    rules={[{ required: true, message: 'Please input your new game name!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`release`}
                    label={`Release`}
                    rules={[{ required: true, message: 'Please input your new game release year!' }]}
                >
                    <Input type="number" min="0" placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`platform`}
                    label={`Platform`}
                    rules={[{ required: true, message: 'Please input your new game platform!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`genre`}
                    label={`Genre`}
                    rules={[{ required: true, message: 'Please input your new game genre!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`image_url`}
                    label={`Image URL`}
                    rules={[
                        {
                            type: "url",
                            message: "This field must be a valid url."
                        }
                    ]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    label={`Mode`}
                >
                    <Checkbox onChange={handleMp} value="1">Multiplayer</Checkbox>
                    <Checkbox onChange={handleSp} value="1">Singleplayer</Checkbox>
                </Form.Item>
                <Form.Item {...tailLayout} shouldUpdate={true}>
                  {() => (
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                      }
                    >
                      Submit
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>
        </>
    )
}

export default CreateGame;