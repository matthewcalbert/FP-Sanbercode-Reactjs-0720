import React, { useEffect, useContext, useState } from 'react';
import axios from 'axios';
import { PageHeader, Form, Input, Button, Alert, Checkbox, InputNumber, message  } from 'antd';
import { CheckCircleOutlined, CloseCircleOutlined, EditOutlined, DeleteOutlined, DownOutlined, UpOutlined, PlusOutlined } from '@ant-design/icons';
import { Row, Col } from 'antd';
import { Table, Tag, Space } from 'antd';
import { Link } from 'react-router-dom';
import * as moment from 'moment'

function compareByAlph (a, b) { if (a < b) { return -1; } if (a > b) { return 1; } return 0; }

const GamesDataEditor = ()=>{
    document.title = "Games Table";

    const [data,setData] = useState(null);
    const [formCheckbox, setFormCheckbox] = useState({mp:false, sp:false});

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    const fetchData = (callback = ()=>{})=>{
        axios.get(`https://backendexample.sanbersy.com/api/games`)
        .then(res => {
            setData(res.data)
            console.log(res.data)
            callback(res.data);
        });
    }

    useEffect( ()=>{
        forceUpdate({});
        if( data === null ){
            fetchData();
        }
    },[] )

    const DeleteButton = ({itemId})=>{
      const handleDelete = ()=>{
          let id = itemId;
          axios.delete(`https://backendexample.sanbersy.com/api/games/${id}`)
          .then(res => {
              if (res.data === "success"){
                  let newArr = data;
                  newArr = newArr.filter(x => x.id !== parseInt(id));
                  setData([...newArr]);
                  message.success('Game deleted');
                  //refetch method
                  // fetchData();
              }else{
                  message.warning('Failed to delete game');
              }
          });
      }
      return (<a onClick={handleDelete}><DeleteOutlined /></a>)
    }

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            sorter: {
              compare: (a, b) => a.id - b.id,
              multiple: 1,
            },
          },
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
          sorter: (a, b) => compareByAlph(a.name, b.name),
        },
        {
          title: 'Genre',
          dataIndex: 'genre',
          key: 'genre',
        },
        {
          title: 'Multiplayer',
          dataIndex: 'multiplayer',
          key: 'multiplayer',
          render: el => {
              if (el > 0 ){
                  return (
                    <CheckCircleOutlined style={{"color":"green"}}></CheckCircleOutlined>
                  )
              }else{
                return (
                  <CloseCircleOutlined style={{"color":"red"}}></CloseCircleOutlined>
                )
              }
          }
        },
        {
          title: 'Singleplayer',
          dataIndex: 'singlePlayer',
          key: 'singlePlayer',
          render: el => {
              if (el > 0 ){
                  return (
                    <CheckCircleOutlined style={{"color":"green"}}></CheckCircleOutlined>
                  )
              }else{
                return (
                  <CloseCircleOutlined style={{"color":"red"}}></CloseCircleOutlined>
                )
              }
          }
        },
        {
          title: 'Platform',
          key: 'platform',
          dataIndex: 'platform',
        },
        {
          title: 'Release',
          key: 'release',
          dataIndex: 'release',
          sorter: {
            compare: (a, b) => a.release - b.release,
            multiple: 1,
          },
        },
        {
          title: 'Created At',
          key: 'created_at',
          dataIndex: 'created_at',
          sorter: (a, b) => { return moment(a.created_at).unix() - moment(b.created_at).unix()}
        },
        {
          title: 'Updated At',
          key: 'updated_at',
          dataIndex: 'updated_at',
          sorter: (a, b) => { return moment(a.updated_at).unix() - moment(b.updated_at).unix()}
        },
        {
          title: 'Action',
          key: 'action',
          fixed: 'right',
          render: (text, record) => (
            // {record.name}
            <Space size="middle">
              <Link to={'/games/edit/'+record.id}><EditOutlined /></Link>
              <DeleteButton itemId={record.id}></DeleteButton>
            </Space>
          ),
        },
      ];
    
      const handleSearch = values => {
        let param = {...values,...formCheckbox};

        console.log(param);

        const filterate = (x)=>{
            if( param.name !== undefined ){
                if( x.name.toLowerCase().search(param.name.toLowerCase()) === -1 )return;
            }
            if( param.release_start !== undefined && !isNaN(parseFloat(param.release_start)) ){
                if( parseInt(x.release) < parseInt(param.release_start) )return;
            }
            if( param.release_end !== undefined && !isNaN(parseFloat(param.release_end)) ){
                if( parseInt(x.release) > parseInt(param.release_end)  )return;
            }
            if( param.genre !== undefined ){
                if( x.genre.toLowerCase().search(param.genre.toLowerCase()) === -1 )return;
            }
            if( param.platform !== undefined ){
                if( x.platform.toLowerCase().search(param.platform.toLowerCase()) === -1 )return;
            }
            if( param.mp && !x.multiplayer ) return;
            if( param.sp && !x.singlePlayer ) return;
            return x;
        }

        fetchData(function(d){
            let newArr = d;
            newArr = newArr.filter(x => filterate(x));
            setData([...newArr]);
        });
        //refetch method
        // fetchData(); 
      };

      const handleMp = e =>{
          setFormCheckbox({...formCheckbox, mp: e.target.checked})
      }
      const handleSp = e =>{
          setFormCheckbox({...formCheckbox, sp: e.target.checked})
      }

    return (
        <>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Games Table"
          >
          </PageHeader>
            <Form
            form={form}
            name="advanced_search"
            className="ant-advanced-search-form"
            onFinish={handleSearch}
            >
                <Row gutter={24}>
                    <Col span={24}>
                        <Form.Item
                            name={`name`}
                            label={`Name`}
                        >
                            <Input placeholder="Search games..." />
                        </Form.Item>
                    </Col>
                    <Col>
                      <Form.Item
                          label={`Release Year`}
                      >
                        <Input.Group compact>
                          <Form.Item
                              name={`release_start`}
                              style={{margin:0}}
                              rules={[({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if ( (value && getFieldValue('release_end') ) && value > getFieldValue('release_end') ) {
                                    return Promise.reject('Min cannot exceed max');
                                  }
                                  return Promise.resolve();
                                },
                              }),]}
                          >
                            <Input style={{ width: 70, textAlign: 'center', borderRight:0 }} placeholder="Min" type="number" min="0" />
                                
                          </Form.Item>
                          <Input
                            className="site-input-split"
                            style={{
                              width: 30,
                              borderLeft: 0,
                              borderRight: 0,
                              pointerEvents: 'none',
                              backgroundColor: '#fff'
                            }}
                            placeholder="~"
                            disabled
                          />
                          <Form.Item
                              name={`release_end`}
                              style={{margin:0}}
                              rules={[({ getFieldValue }) => ({
                                validator(rule, value) {
                                  if ( (value && getFieldValue('release_start') ) && value < getFieldValue('release_start') ) {
                                    return Promise.reject('Max cannot be less than min');
                                  }
                                  return Promise.resolve();
                                },
                              }),]}
                          >
                            <Input
                              className="site-input-right"
                              style={{
                                width: 70,
                                borderLeft: 0,
                                textAlign: 'center',
                              }}
                              placeholder="Max" type="number" min="0"
                            />
                          </Form.Item>
                        </Input.Group>
                      </Form.Item>
                    </Col>
                    <Col>
                        <Form.Item
                            name={`platform`}
                            label={`Platform`}
                        >
                            <Input placeholder="All" />
                        </Form.Item>
                    </Col>
                    <Col span={24}>
                        <Form.Item
                            label={`Mode`}
                        >
                        <Checkbox onChange={handleMp} value="1">Multiplayer</Checkbox>
                        <Checkbox onChange={handleSp} value="1">Singleplayer</Checkbox>
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                    <Button type="primary" htmlType="submit">
                        Search
                    </Button>
                    <Button
                        style={{ margin: '0 8px' }}
                        onClick={() => {
                        form.resetFields();
                        }}
                    >
                        Clear
                    </Button>
                    <Button type="primary" htmlType="button">
                        <Link to="/games/create"><PlusOutlined /> Add Games</Link>
                    </Button>
                    </Col>
                </Row>
            </Form>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col span="12">
            </Col>
            <Col style={{"minWidth":"400px"}}>
                <Table dataSource={data} columns={columns}  scroll={{x:1000, y: 400 }}/>
            </Col>
          </Row>
        </div>
        </>
    )
}

export default GamesDataEditor;