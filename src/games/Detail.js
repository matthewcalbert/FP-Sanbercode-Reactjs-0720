import React from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import { Tag, Divider, Card, Row, Col, PageHeader, Skeleton } from 'antd';

class GameDetail extends React.Component{
    constructor(props){
        super(props);
        this.gameId = this.props.match.params.id;
        this.state = {data: null, dataLoaded: false};
        if( this.state.data === null ){
            this.fetchData();
        }
        document.title =  "Game #"+this.gameId;
    }

    fetchData = ()=>{
        axios.get(`https://backendexample.sanbersy.com/api/games/${this.gameId}`)
        .then(res => {
            let rdata = res.data;
            this.setState({
                data: rdata,
                dataLoaded: true
            });
        });
    }

    renderPost(){
        if( !this.state.dataLoaded ){
            return (
                <>
                <Card bordered={false}className="post" style={{minWidth:300}}>
                <Skeleton avatar paragraph={{ rows: 4 }} />
                </Card>
                </>
                )
        }
        if( this.state.data !== null ){
            let el = this.state.data;
            document.title = el.name+" | Game";
            let mode_tag = []
            if( el.multiplayer ) mode_tag.push(<Tag color="geekblue">Multiplayer</Tag>)
            if( el.singlePlayer ) mode_tag.push(<Tag color="orange">Singleplayer</Tag>)
            return (<Card key={el.id} bordered={false}className="post">
                        <img src={el.image_url} className="post-image"></img>
                        <p>{mode_tag}</p>
                        <p><b>Genre: </b>{el.genre}</p>
                        <p><b>Platform: </b>{el.platform}</p>
                        <p><b>Release: </b>{el.release}</p>
                    </Card>)
        }
    }

    render(){
        return (
            <>
            <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title={this.state.data !== null && this.state.data.name+" | Game"}
            >
            </PageHeader>
            <Row align="center" justify="center" style={{marginTop:30}}>
              <Col>
              {this.renderPost()}
              </Col>
            </Row>
            </>
        )
    }
}

export default GameDetail;