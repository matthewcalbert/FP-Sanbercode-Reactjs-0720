import React, { useEffect, useContext, useState } from 'react';
import axios from 'axios';
import { PageHeader, Form, Input, Button, Alert, Checkbox, Skeleton, message  } from 'antd';
import { Row, Col } from 'antd';
import { useRouteMatch } from 'react-router-dom';

const EditGame = ()=>{
    
    const matchId = useRouteMatch('/games/edit/:id').params.id;
    document.title = "Edit Game #"+matchId;
    let previousData = null;

    const [formCheckbox, setFormCheckbox] = useState({mp:false, sp:false});
    const [dataLoaded, setDataLoaded] = useState(false);

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();

    useEffect( ()=>{
        if( previousData === null ) fetchData();
        forceUpdate({});
    },[] )

    const fetchData = (callback = ()=>{})=>{
        axios.get(`https://backendexample.sanbersy.com/api/games/${matchId}`)
        .then(res => {
            previousData = res.data;
            form.setFieldsValue({
                name: previousData.name, genre: previousData.genre, release: parseInt(previousData.release), platform: previousData.platform, image_url: previousData.image_url
            })
            setFormCheckbox({sp:previousData.singlePlayer, mp:previousData.multiplayer})
            setDataLoaded(true);
            callback(res.data);
        });
    }

    const handleSubmit = (e)=>{
        let params = {...e, ...formCheckbox}
        axios.put(`https://backendexample.sanbersy.com/api/games/${matchId}`,{
            name: params.name, genre: params.genre, release: parseInt(params.release), platform: params.platform, singlePlayer: params.sp, multiplayer: params.mp, image_url: params.image_url
        })
        .then(res => {
            message.success("Game succesfully edited")
            console.log(res.data);
        });
    }

    const handleMp = e =>{
        setFormCheckbox({...formCheckbox, mp: e.target.checked})
    }
    const handleSp = e =>{
        setFormCheckbox({...formCheckbox, sp: e.target.checked})
    }

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
    const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
    };

    return (
        <>
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title={"Edit Game #"+matchId}
          >
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
                <Form {...layout} form={form} name="create_games" onFinish={handleSubmit} initialValues={{ remember: true }}>
                <Form.Item
                    name={`name`}
                    label={`Name`}
                    rules={[{ required: true, message: 'Please input your new game name!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`release`}
                    label={`Release`}
                    rules={[{ required: true, message: 'Please input your new game release year!' }]}
                >
                    <Input type="number" min="0" placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`platform`}
                    label={`Platform`}
                    rules={[{ required: true, message: 'Please input your new game platform!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`genre`}
                    label={`Genre`}
                    rules={[{ required: true, message: 'Please input your new game genre!' }]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    name={`image_url`}
                    label={`Image URL`}
                    rules={[
                        {
                            type: "url",
                            message: "This field must be a valid url."
                        }
                    ]}
                >
                    <Input placeholder="" />
                </Form.Item>
                <Form.Item
                    label={`Mode`}
                >
                    <Checkbox checked={formCheckbox.mp} onChange={handleMp} value="1">Multiplayer</Checkbox>
                    <Checkbox checked={formCheckbox.sp} onChange={handleSp} value="1">Singleplayer</Checkbox>
                </Form.Item>
                <Form.Item {...tailLayout} shouldUpdate={true}>
                {() => (
                    <Button
                    type="primary"
                    htmlType="submit"
                    disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                    }
                    >
                    Update
                    </Button>
                )}
                </Form.Item>
            </Form>
            </Col>
          </Row>
        </div>
        </>
    );
}

export default EditGame;