import React, { useEffect, useContext, useState } from 'react';
import { useHistory, Link} from 'react-router-dom';
import { AuthContext } from '../AuthContext';
import { PageHeader, Form, Input, Button, Alert } from 'antd';
import { Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';

const ChangePassword = ()=>{
    document.title = "Change Password";

    const history = useHistory();
    const [currentLogin,setLogin] = useContext(AuthContext);

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();
    const [message, setMessage] = useState("");
    
    useEffect(() => {
        forceUpdate({});
    }, []);

    const handleChangePassword = (e)=>{
        if( e.old_password !== "" && e.new_password !== "" && e.confirm_password !== "" ){
            axios.get(`https://backendexample.sanbersy.com/api/users/${JSON.parse(currentLogin).id}`)
            .then(res => {
                let user = res.data;
                if( e.old_password === user.password ){
                    updatePassword(e.new_password);
                    return;
                }
                setMessage(<Alert message="Old password incorrect" type="error" style={{"marginBottom":"10px"}} />);
            })
            .catch( ( error ) => {
                if( error.response.status === 404 ){
                    setMessage(<Alert message="User not found" type="error" style={{"marginBottom":"10px"}} />);
                }
                console.log(error);
              return;
            });
        }
    }

    const updatePassword = (new_password)=>{
        axios.put(`https://backendexample.sanbersy.com/api/users/${JSON.parse(currentLogin).id}`,{
            "password":new_password
        })
        .then(res => {
            setMessage(<Alert message="Password successfully changed" type="success" style={{"marginBottom":"10px"}} />);
        })
        .catch( ( error ) => {
            console.log(error);
        });
    }
    
    return (
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Change Password"
          >
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
              <Form form={form} name="normal_login" className="login-form" onFinish={handleChangePassword} initialValues={{ remember: true }}>
                <Form.Item
                  name="old_password"
                  rules={[{ required: true, message: 'Please input your old password!' }]}
                >
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                    placeholder="Old Password"
                    />
                </Form.Item>
                <Form.Item
                  name="new_password"
                  rules={[{ required: true, message: 'Please input your new password!' }]}
                >
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                    placeholder="New Password"
                    />
                </Form.Item>
                <Form.Item
                  name="confirm_password"
                  dependencies={['password']}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: 'Please confirm your new password!',
                    },
                    ({ getFieldValue }) => ({
                      validator(rule, value) {
                        if (!value || getFieldValue('new_password') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject('The two passwords that you entered do not match!');
                      },
                    }),
                  ]}
                >
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                    placeholder="Confirm New Password"
                    />
                </Form.Item>
                {message}
                <Form.Item shouldUpdate={true}>
                  {() => (
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                      }
                    >
                      Change Password
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>
    )
}

export default ChangePassword;