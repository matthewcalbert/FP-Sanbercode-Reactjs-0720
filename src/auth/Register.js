import React, { useEffect, useContext, useState } from 'react';
import { useHistory, Link} from 'react-router-dom';
import { AuthContext } from '../AuthContext';
import { PageHeader, Form, Input, Button, Alert } from 'antd';
import { Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';

const Register = ()=>{
    document.title = "Register";

    const history = useHistory();
    const [currentLogin,setLogin] = useContext(AuthContext);

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();
    const [errorMessage, setErrorMessage] = useState("");
    
    useEffect(() => {
        forceUpdate({});
    }, []);

    const handleRegister = (e)=>{
        if( e.password !== "" && e.username !== "" && e.confirm_password !== "" ){
            if( e.confirm_password !== e.password ){
                return; 
            }
            axios.post(`https://backendexample.sanbersy.com/api/users`,{
                username: e.username, password: e.password
            }).then(res => {
                let user = res.data;
                localStorage.setItem('session',JSON.stringify({id:user.id, username: user.username}));
                setLogin({id:user.id, username: user.username})
                history.push("/");
            }).error(error=>{
                console.log(error);
                setErrorMessage(<Alert message="Oops something went wrong" type="warning" style={{"marginBottom":"10px"}} />);
            });
        }
    }
    
    return (
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Register"
          >
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
              <Form form={form} name="normal_login" className="login-form" onFinish={handleRegister} initialValues={{ remember: true }}>
                <Form.Item
                  name="username"
                  rules={[{ required: true, message: 'Please create a username!' }]}
                >
                  <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[{ required: true, message: 'Please create a password!' }]}
                >
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                    placeholder="Create Password"
                    />
                </Form.Item>
                <Form.Item
                  name="confirm_password"
                  dependencies={['password']}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: 'Please confirm your password!',
                    },
                    ({ getFieldValue }) => ({
                      validator(rule, value) {
                        if (!value || getFieldValue('password') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject('The two passwords that you entered do not match!');
                      },
                    }),
                  ]}
                >
                    <Input.Password prefix={<LockOutlined className="site-form-item-icon" />}
                    placeholder="Confirm Password"
                    />
                </Form.Item>
                {errorMessage}
                <p>Already have an account? <Link to="/login">Login here</Link></p>
                <Form.Item shouldUpdate={true}>
                  {() => (
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                      }
                    >
                      Register
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>
    )
}

export default Register;