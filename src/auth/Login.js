import React, { useEffect, useContext, useState } from 'react';
import { useHistory, Link} from 'react-router-dom';
import { AuthContext } from '../AuthContext';
import { PageHeader, Form, Input, Button, Alert } from 'antd';
import { Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';

const Login = ()=>{
    document.title = "Login";

    const history = useHistory();
    const [currentLogin,setLogin] = useContext(AuthContext);

    const [form] = Form.useForm();
    const [, forceUpdate] = useState();
    const [errorMessage, setErrorMessage] = useState("");
    
    useEffect(() => {
        forceUpdate({});
    }, []);

    const handleLogin = (e)=>{
        if( e.password !== "" && e.username !== "" ){
            axios.post(`https://backendexample.sanbersy.com/api/login`,{
              username: e.username, password: e.password
            })
            .then(res => {
              if(typeof res.data === 'object'){
                let user = res.data;
                localStorage.setItem('session',JSON.stringify({id:user.id, username: user.username}));
                setLogin({id:user.id, username: user.username})
                history.push("/");
              }else{
                setErrorMessage(<Alert message="Wrong password/username" type="error" style={{"marginBottom":"10px"}} />);
              }
                
            })
            .catch( ( error ) => {
              console.log( error );
              return;
            });
        }
    }

    const getUserList = ()=>{
      axios.get(`https://backendexample.sanbersy.com/api/users`)
            .then(res => {
              console.log(res.data)
            })
    }
    
    return (
        <div className="site-page-header-ghost-wrapper">
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Login"
          >
            {/* <Button onClick={getUserList}>Getdata</Button> */}
          </PageHeader>
          <Row justify="center" align="center" style={{"marginTop":"50px"}}>
            <Col style={{"minWidth":"400px"}}>
              <Form form={form} name="normal_login" className="login-form" onFinish={handleLogin} initialValues={{ remember: true }}>
                <Form.Item
                  name="username"
                  rules={[{ required: true, message: 'Please input your username!' }]}
                >
                  <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                </Form.Item>
                <Form.Item
                  name="password"
                  rules={[{ required: true, message: 'Please input your password!' }]}
                >
                  <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                  />
                </Form.Item>
                <p>Don't have an account yet? <Link to="/register">Register here</Link></p>
                {errorMessage}
                <Form.Item shouldUpdate={true}>
                  {() => (
                    <Button
                      type="primary"
                      htmlType="submit"
                      disabled={
                        !form.isFieldsTouched(true) ||
                        form.getFieldsError().filter(({ errors }) => errors.length).length
                      }
                    >
                      Log in
                    </Button>
                  )}
                </Form.Item>
              </Form>
            </Col>
          </Row>
        </div>
    )
}

export default Login;