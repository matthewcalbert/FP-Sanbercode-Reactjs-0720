import React, { useState } from 'react';
import {Link, BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import 'antd/dist/antd.css';
import './index.css';
import { AuthProvider } from './AuthContext';
import MyLayout from './layouts/Layout';


const App = () => {
  return (
    <Router>
      <AuthProvider>
        <MyLayout></MyLayout>
      </AuthProvider>
    </Router>
  );
};

export default App;